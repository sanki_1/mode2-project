package com.academicportalsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.academicportalsystem.model.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer>{

}
