package com.academicportalsystem.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="facultyjpa")
public class Faculty implements Serializable{
	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
private Integer facultyId;
	@Column(name="faculty_name",length=25)
private String facultyName;
private String facultyCourse;
private String emailId;
private String password;
@JsonManagedReference
@OneToMany(mappedBy ="faculty", cascade = { CascadeType.ALL })//variable in address class -bidirection
private List<Batch> batch;
public Faculty() {
	super();
	// TODO Auto-generated constructor stub
}
public Faculty(Integer facultyId, String facultyName, String facultyCourse, String emailId, String password,
		List<Batch> batch) {
	super();
	this.facultyId = facultyId;
	this.facultyName = facultyName;
	this.facultyCourse = facultyCourse;
	this.emailId = emailId;
	this.password = password;
	this.batch = batch;
}
public Integer getFacultyId() {
	return facultyId;
}
public void setFacultyId(Integer facultyId) {
	this.facultyId = facultyId;
}
public String getFacultyName() {
	return facultyName;
}
public void setFacultyName(String facultyName) {
	this.facultyName = facultyName;
}
public String getFacultyCourse() {
	return facultyCourse;
}
public void setFacultyCourse(String facultyCourse) {
	this.facultyCourse = facultyCourse;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public List<Batch> getBatch() {
	return batch;
}
public void setBatch(List<Batch> batch) {
	this.batch = batch;
}

}