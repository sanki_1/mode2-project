package com.academicportalsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="Batch")
public class Batch implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
private Integer batchId;
private String batchName;
private Integer noOfTrainees;
@DateTimeFormat(pattern = "yyyy-MM-dd")
@Temporal(TemporalType.DATE)
private Date batchStartdate;
@DateTimeFormat(pattern = "yyyy-MM-dd")
@Temporal(TemporalType.DATE)
private Date batchEnddate;
@ManyToOne(cascade = CascadeType.ALL)//deleting parent will delete child also
@JoinColumn(name = "faculty_id") // FK
@JsonBackReference
//bi-direction--
private Faculty faculty;
public Batch() {
	super();
	// TODO Auto-generated constructor stub
}
public Batch(Integer batchId, String batchName, Integer noOfTrainees, Date batchStartdate, Date batchEnddate,
		Faculty faculty) {
	super();
	this.batchId = batchId;
	this.batchName = batchName;
	this.noOfTrainees = noOfTrainees;
	this.batchStartdate = batchStartdate;
	this.batchEnddate = batchEnddate;
	this.faculty = faculty;
}
public Integer getBatchId() {
	return batchId;
}
public void setBatchId(Integer batchId) {
	this.batchId = batchId;
}
public String getBatchName() {
	return batchName;
}
public void setBatchName(String batchName) {
	this.batchName = batchName;
}
public Integer getNoOfTrainees() {
	return noOfTrainees;
}
public void setNoOfTrainees(Integer noOfTrainees) {
	this.noOfTrainees = noOfTrainees;
}
public Date getBatchStartdate() {
	return batchStartdate;
}
public void setBatchStartdate(Date batchStartdate) {
	this.batchStartdate = batchStartdate;
}
public Date getBatchEnddate() {
	return batchEnddate;
}
public void setBatchEnddate(Date batchEnddate) {
	this.batchEnddate = batchEnddate;
}
public Faculty getFaculty() {
	return faculty;
}
public void setFaculty(Faculty faculty) {
	this.faculty = faculty;
}

}