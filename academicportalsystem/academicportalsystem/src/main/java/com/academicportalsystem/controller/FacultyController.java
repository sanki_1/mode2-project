package com.academicportalsystem.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.academicportalsystem.model.Faculty;
import com.academicportalsystem.service.FacultyService;
@CrossOrigin("*")
@RestController
@RequestMapping(value="/api")
public class FacultyController {
	@Autowired
private FacultyService facultyService; //has a ==wired==di
	@GetMapping(value="/faculty")//all
	public List<Faculty> list()
	{
		return facultyService.listAll(); 
	}
	@GetMapping(value="faculty/id/{data}")//read
	public Faculty read(@PathVariable("data") Integer id)
	{
		return facultyService.readByFaculty(id) ;
	}
	@PostMapping(value="/faculty")//save
	public ResponseEntity<Faculty>save(@RequestBody Faculty faculty) throws Exception
	{
		
		return new  ResponseEntity<Faculty>(facultyService.save(faculty),HttpStatus.OK);
	}
	@PutMapping(value="/faculty/update")
	public ResponseEntity<Faculty>update(@RequestBody Faculty faculty)
	{
		return new  ResponseEntity<Faculty>(facultyService.save(faculty),HttpStatus.OK);
	}
	@DeleteMapping(value="faculty/{data}")//delete
	public Integer delete(@PathVariable("data") Integer id)
	{
		return facultyService.delete(id) ;
	}
	
	
	
}
	


