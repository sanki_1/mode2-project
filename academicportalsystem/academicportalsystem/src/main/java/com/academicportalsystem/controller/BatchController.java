package com.academicportalsystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academicportalsystem.model.Batch;
import com.academicportalsystem.model.Faculty;
import com.academicportalsystem.service.BatchService;
import com.academicportalsystem.service.FacultyService;

@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping(value="/api")
public class BatchController {
	@Autowired
	private BatchService batchService; //has a ==wired==di
		@GetMapping(value="batch")//all
		public List<Batch> list()
		{
			return batchService.listAll(); 
		}
		@GetMapping(value="/batch/{batchId}")//read/{data}
		public Batch read(@PathVariable("batchId") Integer id)
		{
			return batchService.readByBatch(id) ;
		}
		@PostMapping(value="/batch")//save
		public ResponseEntity<Batch>save(@RequestBody Batch batch)
		{
			return new  ResponseEntity<Batch>(batchService.save(batch),HttpStatus.OK);
		}
		@PutMapping(value="update")
		public ResponseEntity<Batch>update(@RequestBody Batch batch)
		{
			return new  ResponseEntity<Batch>(batchService.save(batch),HttpStatus.OK);
		}
		@DeleteMapping(value="delete/{data}")
		public Integer delete(@PathVariable("data") Integer id)
		{
			return batchService.delete(id) ;
		}
}
