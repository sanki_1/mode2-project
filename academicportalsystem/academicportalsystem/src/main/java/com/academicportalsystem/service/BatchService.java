package com.academicportalsystem.service;

import java.util.List;

import com.academicportalsystem.model.Batch;
import com.academicportalsystem.model.Faculty;

public interface BatchService {
	
		public abstract List<Batch> listAll();//r
		public abstract  Batch readByBatch(Integer abc );
		public abstract  Batch save(Batch batch);//c u
		public abstract  Integer delete(Integer efg);//d
}

