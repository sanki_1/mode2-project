package com.academicportalsystem.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.academicportalsystem.dao.FacultyRepository;
import com.academicportalsystem.model.Faculty;
@Service
public class FacultyServiceImpl implements FacultyService{
	@Autowired
private FacultyRepository facultyRepository;//wire==DI
	
	@Override
	@Transactional
	public List<Faculty> listAll() {
		
		return facultyRepository.findAll();
	}

	@Transactional
	public Faculty readByFaculty(Integer abc) {
		Faculty faculty=null;
		Optional<Faculty> optional=facultyRepository.findById(abc);
		if(optional.isPresent())
		{
			faculty=optional.get();//local
		}
		else
		{
			System.out.println("No such employeee");
		}
		return faculty;
		
	}

	@Transactional
	public Faculty save(Faculty faculty) {
		return facultyRepository.save(faculty);
		
	}

	@Transactional
	public Integer delete(Integer efg) {
		facultyRepository.deleteById(efg);
		return efg;
	
	}

	

	
	
}
