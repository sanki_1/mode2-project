package com.academicportalsystem.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.academicportalsystem.dao.BatchRepository;
import com.academicportalsystem.dao.FacultyRepository;
import com.academicportalsystem.model.Batch;
import com.academicportalsystem.model.Faculty;
@Service
public class BatchServiceImpl implements BatchService{
	@Autowired
private BatchRepository batchRepository;//wire==DI
	
	@Override
	@Transactional
	public List<Batch> listAll() {
		
		return batchRepository.findAll();
	}

	@Transactional
	public Batch readByBatch(Integer abc) {
		Batch batch=null;
		Optional<Batch> optional=batchRepository.findById(abc);
		if(optional.isPresent())
		{
			batch=optional.get();//local
		}
		else
		{
			System.out.println("No such employeee");
		}
		return batch;
		
	}

	@Transactional
	public Batch save(Batch batch) {
		return batchRepository.save(batch);
		
	}

	@Transactional
	public Integer delete(Integer efg) {
		batchRepository.deleteById(efg);
		return efg;
	
	}

}
