import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { AuthguardService } from '../authguard.service';
import { Faculty } from '../faculty';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username='';
  password='';
  invalidLogin=false;
    constructor(private loginservice:AuthenticationService,private router:Router) { }
  
    ngOnInit() {
    }
  
    checkLogin() {
      if (this.loginservice.authenticate(this.username, this.password)) {
        this.router.navigate([''])
        this.invalidLogin = false
      } else
        this.invalidLogin = true
    }
  }


