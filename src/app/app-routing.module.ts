import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddbatchComponent } from './addbatch/addbatch.component';
import { AuthguardService } from './authguard.service';

import { EditFacultyComponent } from './edit-faculty/edit-faculty.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterfacultyComponent } from './registerfaculty/registerfaculty.component';
import { SearchFacultyComponent } from './search-faculty/search-faculty.component';

import { ViewbatchdetailsComponent } from './viewbatchdetails/viewbatchdetails.component';
import { ViewfacultyComponent } from './viewfaculty/viewfaculty.component';


const routes: Routes = [
  {
       path:'add',
       component:RegisterfacultyComponent 
  },
  {
    path:'addbatch',
    component:AddbatchComponent
  },
  {
    path:'faculty',
    component:ViewfacultyComponent,
    
  },
  {
    path:'batch',
    component:ViewbatchdetailsComponent
  },
  
  {
  path:'edit',
  component:EditFacultyComponent
  } ,
  {
    path:'search',
  component:SearchFacultyComponent
  },
  {
    path:'login',
  component:LoginComponent
  
  },
  {
    path:'logout',
  component:LogoutComponent,
  canActivate:[AuthguardService]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
