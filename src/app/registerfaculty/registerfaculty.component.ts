import { Component, OnInit } from '@angular/core';
import { Faculty } from '../faculty';
import { FacultyService } from '../faculty.service';

@Component({
  selector: 'app-registerfaculty',
  templateUrl: './registerfaculty.component.html',
  styleUrls: ['./registerfaculty.component.css']
})
export class RegisterfacultyComponent implements OnInit {
  faculty:Faculty=new Faculty();
  submitted=false;
  constructor(private facultyService:FacultyService) { }

  ngOnInit() {
  }
  onSubmit()
  {
  console.log("Successfully Submitted");
  this.save();
  }
  
   save() {
  this.facultyService.saveFaculty(this.faculty)
  .subscribe(
  data => {
  console.log(data);
  this.submitted = true;
  },
  error => console.log(error));
  this.faculty = new Faculty();
  }
  
   newFaculty(): void {
  this.submitted = false;
  this.faculty = new Faculty();
  }
  }

