import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Faculty } from './faculty';

@Injectable({
  providedIn: 'root'
})
export class FacultyService {
  private baseUrl = 'http://localhost:9095/api/faculty';
  constructor(private http:HttpClient)
  {

  }


saveFaculty(faculty: any): Observable<any> {
  console.log(faculty);
return this.http.post(this.baseUrl, faculty);
console.log(faculty);
}
getFaculties(): Observable<any> {
  return this.http.get(this.baseUrl);
}
getFaculty(id:number):Observable<any>
{
  return this.http.get(`${this.baseUrl}/${id}`);
}
updateFaculty(faculty: Object): Observable<Object> {
  console.log(faculty);
  return this.http.put(`${this.baseUrl}` + `/update`, faculty);
 }
 deleteFaculty(id: number): Observable<any> {
  return this.http.delete(`${this.baseUrl}/${id}`);
}
getFacultiesById(facultyId: number): Observable<any> {
  return this.http.get(`${this.baseUrl}/id/${facultyId}`);
}
}

