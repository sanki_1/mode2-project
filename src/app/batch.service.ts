import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BatchService {

  constructor(private http:HttpClient) { }
  private baseUrl = 'http://localhost:9095/api/batch';
  saveBatch(batch: any): Observable<any> {
    console.log(batch);
  return this.http.post(this.baseUrl, batch);
  console.log(batch);
  }
  getBatches(): Observable<any> {
    return this.http.get(this.baseUrl);
  }
  getBatchbyid(batchId: number): Observable<any> {
    console.log(batchId);
   // return this.http.get(`${this.baseUrl}/${batchId}`);
   return this.http.get(`${this.baseUrl}/${batchId}`);
  }
}
