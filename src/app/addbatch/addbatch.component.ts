import { Component, OnInit } from '@angular/core';
import { Batch } from '../batch';
import { BatchService } from '../batch.service';

@Component({
  selector: 'app-addbatch',
  templateUrl: './addbatch.component.html',
  styleUrls: ['./addbatch.component.css']
})
export class AddbatchComponent implements OnInit {
  batch:Batch=new Batch();
  submitted=false;
  constructor(private batchService:BatchService) { }

  ngOnInit() {
  }

  onSubmit()
  {
  console.log("Successfully Submitted");
  this.save();
  }
  
   save() {
  this.batchService.saveBatch(this.batch)
  .subscribe(
  data => {
  console.log(data);
  this.submitted = true;
  },
  error => console.log(error));
  this.batch = new Batch();
  }
  
   newBatch(): void {
  this.submitted = false;
  this.batch = new Batch();
  }
}
