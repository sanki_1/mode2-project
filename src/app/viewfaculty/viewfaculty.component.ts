import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Faculty } from '../faculty';
import { FacultyService } from '../faculty.service';

@Component({
  selector: 'app-viewfaculty',
  templateUrl: './viewfaculty.component.html',
  styleUrls: ['./viewfaculty.component.css']
})
export class ViewfacultyComponent implements OnInit {
  faculty:Observable<Faculty[]>;
  constructor(private facultyService:FacultyService,private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData()
{
this.faculty=this.facultyService.getFaculties();
}
editFaculty(faculty: Faculty):void {
  console.log("into edit");
  localStorage.setItem("id",faculty.facultyId.toString());
  this.router.navigate(["edit"]);
}
deleteFaculty(faculty:Faculty) {
  this.facultyService.deleteFaculty(faculty.facultyId)
    .subscribe(
      data => {
        console.log(data);
       this.reloadData();
      
      },
      error => console.log(error));
}
}
