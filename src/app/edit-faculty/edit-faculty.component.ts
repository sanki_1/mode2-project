import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Faculty } from '../faculty';
import { FacultyService } from '../faculty.service';

@Component({
  selector: 'app-edit-faculty',
  templateUrl: './edit-faculty.component.html',
  styleUrls: ['./edit-faculty.component.css']
})
export class EditFacultyComponent implements OnInit {
  faculty:Faculty=new Faculty();
  constructor(private facultyService:FacultyService,private router:Router) { }

  ngOnInit() {
    this.editFaculty();
  }
  editFaculty()
  {
    let id=localStorage.getItem("facultyId");
    this.facultyService.getFaculty(+id)  //convert string to number
     .subscribe(data=>{
            this.faculty=data;
        })

  }
  onUpdate(){
    console.log("into update");
    this.facultyService.updateFaculty(this.faculty)
          .subscribe(data => {
     console.log(data);
     this.router.navigate(["faculty"]);
    }, error => console.log(error));
        this.faculty = new Faculty();
    }
  }


