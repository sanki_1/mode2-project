import { Component, OnInit } from '@angular/core';
import { Faculty } from '../faculty';
import { FacultyService } from '../faculty.service';

@Component({
  selector: 'app-search-faculty',
  templateUrl: './search-faculty.component.html',
  styleUrls: ['./search-faculty.component.css']
})
export class SearchFacultyComponent implements OnInit {
  faculties:Faculty[];
  constructor(private facultyService:FacultyService) { }

  ngOnInit() {
  }
  facultyId:number;
  onSubmit()
  {
    this.searchFaculties();
  }
  submitted=false;
  searchFaculties() {
    this.faculties = [];
    this.facultyService.getFacultiesById(this.facultyId)
      .subscribe(
        data => {
          this.faculties=data;
          this.submitted = true;
        },
      );
  }
}
