import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RegisterfacultyComponent } from './registerfaculty/registerfaculty.component';
import { FormsModule } from '@angular/forms';
import { AddbatchComponent } from './addbatch/addbatch.component';
import { ViewfacultyComponent } from './viewfaculty/viewfaculty.component';
import { ViewbatchdetailsComponent } from './viewbatchdetails/viewbatchdetails.component';


import { EditFacultyComponent } from './edit-faculty/edit-faculty.component';
import { SearchFacultyComponent } from './search-faculty/search-faculty.component';
import { LogoutComponent } from './logout/logout.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterfacultyComponent,
    AddbatchComponent,
    ViewfacultyComponent,
    ViewbatchdetailsComponent,
    
   
    EditFacultyComponent,
    
   
    SearchFacultyComponent,
    
   
    LogoutComponent,
    
   
    LoginComponent,
    
   
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
