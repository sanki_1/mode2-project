import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewbatchdetailsComponent } from './viewbatchdetails.component';

describe('ViewbatchdetailsComponent', () => {
  let component: ViewbatchdetailsComponent;
  let fixture: ComponentFixture<ViewbatchdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewbatchdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewbatchdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
