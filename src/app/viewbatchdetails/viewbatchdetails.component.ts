import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Batch } from '../batch';
import { BatchService } from '../batch.service';

@Component({
  selector: 'app-viewbatchdetails',
  templateUrl: './viewbatchdetails.component.html',
  styleUrls: ['./viewbatchdetails.component.css']
})
export class ViewbatchdetailsComponent implements OnInit {
  batch:Observable<Batch[]>;
  constructor(private batchService:BatchService,private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData()
  {
  this.batch=this.batchService.getBatches();
  }
}
